package ec.com.quintosa.images;

import clarifai2.api.ClarifaiBuilder;
import clarifai2.api.ClarifaiClient;
import clarifai2.dto.input.ClarifaiInput;
import clarifai2.dto.model.output.ClarifaiOutput;
import clarifai2.dto.prediction.Color;
import clarifai2.dto.prediction.Concept;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * https://www.clarifai.com/models
 *
 * https://www.clarifai.com/developer/reference
 */
public class ClarifaiSimpleApi {

    // create your api key here
    // https://portal.clarifai.com/
    static String apiKey = "your api key";

    @NotNull
    final private static Logger logger = LoggerFactory.getLogger(ClarifaiSimpleApi.class);

    static List<ClarifaiOutput<Concept>> getImageGeneralInfo(byte[] imageBytes) {
        final ClarifaiClient client = new ClarifaiBuilder(apiKey)
                .client(new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .addInterceptor(new HttpLoggingInterceptor(logger::info).setLevel(HttpLoggingInterceptor.Level.BASIC))
                        .build()
                )
                .buildSync();
        return client.getDefaultModels().generalModel().predict().withInputs(
                ClarifaiInput.forImage(imageBytes)
        ).executeSync().get();
    }

    static List<ClarifaiOutput<Color>> getImageColorsInfo(byte[] imageBytes) {
        final ClarifaiClient client = new ClarifaiBuilder(apiKey)
                .client(new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .addInterceptor(new HttpLoggingInterceptor(logger::info).setLevel(HttpLoggingInterceptor.Level.BASIC))
                        .build()
                )
                .buildSync();
        return client.getDefaultModels().colorModel().predict().withInputs(
                ClarifaiInput.forImage(imageBytes)
        ).executeSync().get();
    }

    static List<ClarifaiOutput<Concept>> getImageApparelInfo(byte[] imageBytes) {
        final ClarifaiClient client = new ClarifaiBuilder(apiKey)
                .client(new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .addInterceptor(new HttpLoggingInterceptor(logger::info).setLevel(HttpLoggingInterceptor.Level.BASIC))
                        .build()
                )
                .buildSync();
        return client.getDefaultModels().apparelModel().predict().withInputs(
                ClarifaiInput.forImage(imageBytes)
        ).executeSync().get();
    }
}
