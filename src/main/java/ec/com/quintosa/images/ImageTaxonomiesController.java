package ec.com.quintosa.images;

import clarifai2.dto.model.output.ClarifaiOutput;
import clarifai2.dto.prediction.Color;
import clarifai2.dto.prediction.Concept;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * note: in postman remove header items/key before make the petitions for send images
 */
@Controller    // This means that this class is a Controller
@RequestMapping("/image") // This means URL's start with /image (after Application path context)
public class ImageTaxonomiesController {

    @PostMapping(path = "/info") // Map ONLY POST Requests
    public @ResponseBody
    Map<String, Float> info(@RequestParam("image") MultipartFile image) throws IOException {
        final List<ClarifaiOutput<Concept>> imageGeneralInfo = ClarifaiSimpleApi.getImageGeneralInfo(
                image.getBytes()
        );
        final Map<String, Float> result = new HashMap<>();
        imageGeneralInfo.forEach(r -> r.data().forEach(d -> result.put(d.name(), d.value())));
        return result;
    }

    @PostMapping(path = "/colors") // Map ONLY POST Requests
    public @ResponseBody
    Map<String, Float> colors(@RequestParam("image") MultipartFile image) throws IOException {
        final List<ClarifaiOutput<Color>> imageGeneralInfo = ClarifaiSimpleApi.getImageColorsInfo(
                image.getBytes()
        );
        final Map<String, Float> result = new HashMap<>();
        imageGeneralInfo.forEach(r -> r.data().forEach(d -> result.put(d.webSafeColorName(), d.value())));
        return result;
    }

    @PostMapping(path = "/apparel") // Map ONLY POST Requests
    public @ResponseBody
    Map<String, Float> apparel(@RequestParam("image") MultipartFile image) throws IOException {
        final List<ClarifaiOutput<Concept>> imageGeneralInfo = ClarifaiSimpleApi.getImageApparelInfo(
                image.getBytes()
        );
        final Map<String, Float> result = new HashMap<>();
        imageGeneralInfo.forEach(r -> r.data().forEach(d -> result.put(d.name(), d.value())));
        return result;
    }
}