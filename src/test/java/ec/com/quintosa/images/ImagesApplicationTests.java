package ec.com.quintosa.images;

import clarifai2.api.ClarifaiBuilder;
import clarifai2.api.ClarifaiClient;
import clarifai2.api.request.model.PredictRequest;
import clarifai2.dto.input.ClarifaiInput;
import clarifai2.dto.model.Model;
import clarifai2.dto.model.output.ClarifaiOutput;
import clarifai2.dto.prediction.Concept;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ImagesApplicationTests {

    @NotNull
    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void testImageInfo() {
        final ClarifaiClient client = new ClarifaiBuilder(ClarifaiSimpleApi.apiKey)
                .client(new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .addInterceptor(new HttpLoggingInterceptor(logger::info).setLevel(HttpLoggingInterceptor.Level.BASIC))
                        .build()
                )
                .buildSync();

        Model<Concept> generalModel = client.getDefaultModels().generalModel();

        PredictRequest<Concept> request = generalModel.predict().withInputs(
                ClarifaiInput.forImage("https://samples.clarifai.com/metro-north.jpg")
        );
        List<ClarifaiOutput<Concept>> result = request.executeSync().get();

        result.forEach(r -> {
            r.data().forEach(d -> {
                System.out.println(d.name() + " - " + d.value());
            });
        });
        System.out.println(result.toString());
    }
}
